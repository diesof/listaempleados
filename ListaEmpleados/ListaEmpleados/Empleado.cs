﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListaEmpleados
{
    class Empleado
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { set; get; }
        public string ApellidoMaterno { set; get; }
        public string Puesto { set; get; }
        public int Edad { set; get; }
        public double Sueldo { set; get; }

        Empleado empleado;
        private List<Empleado> listaEmpleados = new List<Empleado>();
        private int cantidadEmpleados;
        private int numeroEmpleado;

        public void SolicitarCantidadDatosEmpleados()
        {
            Console.Write("¿Cuántos empleados dará de alta?: ");
            cantidadEmpleados = int.Parse(Console.ReadLine());
        }

        public void SolicitarDatosEmpleado()
        {
            for (numeroEmpleado = 1; numeroEmpleado <= cantidadEmpleados; numeroEmpleado++)
            {
                Console.WriteLine($"\n\n**********Empleado {numeroEmpleado}**********");

                empleado = new Empleado();

                Console.Write("Ingrese el nombre del empleado: ");
                empleado.Nombre = Console.ReadLine();

                Console.Write($"Ingrese el apellido paterno de {empleado.Nombre}: ");
                empleado.ApellidoPaterno = Console.ReadLine();

                Console.Write($"Ingrese el apellido materno de {empleado.Nombre}: ");
                empleado.ApellidoMaterno = Console.ReadLine();

                Console.Write($"Ingrese el puesto de {empleado.Nombre}: ");
                empleado.Puesto = Console.ReadLine();

                Console.Write($"Ingrese la edad de {empleado.Nombre}: ");
                empleado.Edad = int.Parse(Console.ReadLine());

                Console.Write($"Ingrese el sueldo de {empleado.Nombre}: ");
                empleado.Sueldo = double.Parse(Console.ReadLine());

                listaEmpleados.Add(empleado);
            }
        }

        /*Ordenar lista de empleados de acuerdo al sueldo(menor a mayor)*/
        private List<Empleado> OrdenarListaEmpleados(List<Empleado> lista)
        {
            var empleadosOrdenados = lista.OrderBy(o => o.Sueldo).ToList();

            return empleadosOrdenados;
        }

        public void MostrarDatosEmpleado()
        {
            numeroEmpleado = 1;

            foreach (var empleados in OrdenarListaEmpleados(listaEmpleados))
            {
                Console.WriteLine($"\n\n<<<<<<<<<<Empleado {numeroEmpleado}>>>>>>>>>>");

                Console.WriteLine($"Nombre: {empleados.Nombre.ToUpper()}");
                Console.WriteLine($"Apellido Paterno: {empleados.ApellidoPaterno.ToUpper()}");
                Console.WriteLine($"Apellido Materno: {empleados.ApellidoMaterno.ToUpper()}");
                Console.WriteLine($"Puesto: {empleados.Puesto.ToUpper()}");
                Console.WriteLine($"Edad: {empleados.Edad}");
                Console.WriteLine($"Sueldo: {empleados.Sueldo.ToString("C")}");

                numeroEmpleado += 1;
            }
        }
    }
}